class Hotel < ActiveRecord::Base
  def self.search(search)
    if search
      self.where("location like ?", "%#{search}%")
    else
      self.all
    end
  end
end
