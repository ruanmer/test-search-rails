class AddUnavailableToHotels < ActiveRecord::Migration
  def change
    add_column :hotels, :unavailable, :string
  end
end
